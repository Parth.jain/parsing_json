package com.example.android.parsing_json;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BufferedReader reader = null;
       /* String mLine=getStringFromJson(reader);
        Log.i("MainActivity",mLine);
        try {
            JSONObject jsonObject=new JSONObject(mLine);
            JSONObject firstObj=jsonObject.getJSONObject("glossary");
            String firstObjValue=firstObj.getString("title");
            Log.i("first obj value",firstObjValue);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        try {
            JSONObject jsonObject=new JSONObject(loadJSONFromAsset());
            JSONObject jo_inside= jsonObject.getJSONObject("glossary");
         //   JSONObject j2=jo_inside.getJSONObject("title");
            String val_title=jo_inside.getString("title");
            Log.e("MainAct",val_title);
            JSONObject gloss_div=jo_inside.getJSONObject("GlossDiv");
           // JSONObject title2=gross_div.getJSONObject("title");
            String title_of_jsonobj2=gloss_div.getString("title");
            Log.e("MainAct",title_of_jsonobj2);
            JSONObject GlossList=gloss_div.getJSONObject("GlossList");
            JSONObject GlossEntry=GlossList.getJSONObject("GlossEntry");
            String ID=GlossEntry.getString("ID");
            Log.e("MainAct",ID);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is =getAssets().open("file1.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
   /* String getStringFromJson(BufferedReader reader){
        String json = null;
        try {
            InputStream is = getAssets().open("file1.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }*/
       /* String mLine = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("file1.json")));

            // do reading, usually loop until end of file reading
           
            while ((mLine = reader.readLine()) != null) {
                //process line
                //Log.i("MainActivity",mLine);
            }
        } catch (IOException e) {
            //log the exception
            Log.i("MainActivity","File not readed");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return mLine;
    }*/

}
